/*
 * This source file is part of jCandlePlay
 * 
 * jCandlePlay is free software: you can redistribute it
 * and/or modify it under the terms of the MIT License.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.jcandleplay.graph;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.plaf.metal.MetalScrollBarUI;

/**
 * This class represents a Custom UI Component for JScroll
 * @author Felipe Santos
 *
 */
public class CustomScrollBarUI extends MetalScrollBarUI {
	
	static class ButtonPlux extends JButton {
		private static final long serialVersionUID = 1L;
		private Image imageBtn;

		public ButtonPlux(int width, int height) {
			try {
				URL resource = CustomScrollBarUI.class.getResource("plusbtn.png");
				imageBtn = ImageIO.read(resource);
			} catch (IOException e) {
				e.printStackTrace();
			}

			setPreferredSize(new Dimension(width, height));
		}

		@Override
		protected void paintComponent(Graphics g) {
			g.drawImage(imageBtn, 0, 0, null);
		}
	}

	static class ButtonMinus extends JButton {
		private static final long serialVersionUID = 1L;
		private Image imageBtn;

		public ButtonMinus(int width, int height) {
			try {
				URL resource = CustomScrollBarUI.class.getResource("minusbtn.png");
				imageBtn = ImageIO.read(resource);
			} catch (IOException e) {
				e.printStackTrace();
			}

			setPreferredSize(new Dimension(width, height));
		}
		
		@Override
		protected void paintComponent(Graphics g) {
			g.drawImage(imageBtn, 0, 0, null);
		}
	}
	
	///////////////////////////////////////////////////////////////////////////

	private int width;
	
	private int height;
	
	private boolean inverted;
	
	public CustomScrollBarUI(int width, int height) {
		this(width, height, false);
	}
	
	public CustomScrollBarUI(int width, int height, boolean inverted) {
		this.width = width;
		this.height = height;
		this.inverted = inverted;
	}

	@Override
	protected JButton createIncreaseButton(int orientation) {
		if (inverted) {
			return new ButtonMinus(width, height);			
		} else {
			return new ButtonPlux(width, height);
		}
	}

	@Override
	protected JButton createDecreaseButton(int orientation) {
		if (inverted) {
			return new ButtonPlux(width, height);
		} else {
			return new ButtonMinus(width, height);			
		}
	}
}